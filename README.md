# Hands-On Session on Machine Learning
Exotics Workshop at Rome

Dinos Bachas, Rome 29 May - 1 June 2018
## Set up your system
For this hands-on session I will be using Jupyter Notebooks. A Jupyter Notebook is a nice web application where you have everything in one browser view; code, text, visualization. It supports over 40 programming languages.

Link: http://jupyter.org/


### Using SWAN (preferred solution, used here):
1. With your preferred browser, log in at [swan.cern.ch](https://swan.cern.ch) and select the software stack 93. Click **'Start My Session'**. You need to have a CERNbox account for this to work. However if you don't you will be prompt to create one.
2. On the top right of the SWAN web-page, open a terminal from the '\>\_' icon and clone this repository:
git clone https://gitlab.cern.ch/kbachas/RomeExoticsWorkshopHandsOnML.git
3. Use your web-browser under the SWAN interface window to go to the notebook (IntroToDNN.ipynb) in the downloaded repository. Click on the notebook and it should start with all needed libraries pre-loaded. No need to download or install anything!
Alternatively, if you are familiar with CERNbox, you can open the web interface of your CERNbox account at https://cernbox.cern.ch/ and navigate to the Jupyter notebook in the repository you've downloaded. Next to it you should see an 'Open in SWAN' orange-black button. Click this and it should get you started.

### Manual installation of ML-packages on your preferred machine.
To install the needed software packages and libraries on your laptop or preferred machine, you can use the approach of first installing a package manager like Miniconda or Anaconda:

https://www.anaconda.com/download/

https://conda.io/docs/

This will enable you to build a kind of 'virtual machine' where you can install whatever packages you need keeping your machine's default setup and configuration intact.

Installation instructions are available on the Machine Learning Forum Twiki:
https://twiki.cern.ch/twiki/bin/view/AtlasComputing/MLSoftwareStandAloneSetup

Example of installation process:
1. Download the Miniconda package
   `wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh`
2. Install
   `sh Miniconda3-latest-Linux-x86_64.sh`
3. make sure that
   `export PATH="~/miniconda3/bin:$PATH"` is under your login script
4. After the conda installation is complete, close the terminal and reopen it
5. Add installation channels
   `conda config --add channels https://conda.anaconda.org/NLeSC`
6. To create a Conda environment simply type: `conda create --name=myAIenv`
7. To activate this environment, use: `source activate myAIenv`. This will now give you a characteristic (myAIenv) in front of your prompt.
8. Now under this environment you can choose to install the packages you need with conda install expressions like:
`conda install jupyter numpy matplotlib scikit-learn pandas theano`
and/or then through pip `pip install keras`
9. To deactivate an active environment, use:
`source deactivate`

## Useful tutorials/links on ML

**Much of this hands-on session is inspired from Michela Paganini's excellent tutorial on Deep Neural Networks \[2\].**

There are already many excellent tutorials and material on ML that they are totally worth following them.

\[1\] [https://github.com/dguest/mlbnn][1]

\[2\] [https://github.com/YaleATLAS/CERNDeepLearningTutorial][2]

\[3\] [https://github.com/iml-wg/HEP-ML-Resources][3]

\[4\] [https://github.com/iml-wg/HEP-ML-Resources#tutorials][4]

[1]: https://github.com/dguest/mlbnn
[2]: https://github.com/YaleATLAS/CERNDeepLearningTutorial
[3]: https://github.com/iml-wg/HEP-ML-Resources
[4]: https://github.com/iml-wg/HEP-ML-Resources#tutorials
